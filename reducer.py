#!/usr/bin/python
from operator import itemgetter
import sys

current_term = ""
current_count = 0
term = ""
for line in sys.stdin:
	line = line.strip()
	term, count = line.split("\t",1)
	try:
		count = int(count)
	except:
		pass

	if current_term == term:
		current_count += count
	else:
		if current_term:
			print "%s\t%s" % (current_term, current_count)
		current_count = count
		current_term = term
if current_term == term:
	print "%s\t%s" % (current_term, current_count)

